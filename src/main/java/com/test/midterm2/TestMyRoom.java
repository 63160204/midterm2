/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.midterm2;

/**
 *
 * @author patthamawan
 */
public class TestMyRoom {
    
    public static void main(String[] args) {
        MyRoom myroom = new MyRoom("name","color","type");
        
                
        Doll1 Bobo = new Doll1("Bobo  ","Gray color ","-> Dog");
        Bobo.say();
        Bobo.jump();
        Bobo.jump(2);
        System.out.println("----------------");
        
        
        Doll2 Mumu = new Doll2("Mumu  ","White color ","-> Troll");
        Mumu.say();
        Mumu.jump();
        Mumu.jump(4);
        System.out.println("----------------");
        
        
        
        Doll3 BabyBear = new Doll3("BabyBear  ","White color ","-> Bear");
        BabyBear.say();
        BabyBear.jump();
        BabyBear.jump(3);
        System.out.println("----------------");
        
    }   
}
