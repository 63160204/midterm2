/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.midterm2;

/**
 *
 * @author patthamawan
 */
public class Doll1 extends MyRoom {

    public Doll1(String name, String color, String type) {
        super(name, color, type);
        System.out.println(name + color + type);

    }

    @Override
    public void say() {
        super.say();
        System.out.println("I'm " + name +".");
    }

    @Override
    public void jump() {
        super.jump();
        System.out.println(name+"JUMP!!!");
    }
}
